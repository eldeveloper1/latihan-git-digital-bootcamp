import java.util.Scanner;
/*
	KETERANGAN
	CODE INI DIBUAT OLEH DANIEL
*/
public class JobVacancy{
	public static void main(String[] args) {
		
		try (Scanner userInput = new Scanner(System.in)) {
      /*DEKLARASI*/
      String nama, lulusan, gender, posisi;
      int umur, pengalaman;
      double  penampilan;

      /*OUTPUT PADA LAYAR UNTUK MEMASUKAN INPUT USER*/ 
      System.out.println();
      System.out.println("///////////////////////////////////////////////////////////////////////////////");
      System.out.println("MASUKAN DATA DIRI ANDA :");
      System.out.println();

      System.out.println("Nama :");
      nama = userInput.next();
      System.out.println("Umur :");
      umur = userInput.nextInt();
      System.out.println("Jenis Kelamin (Pria/Wanita) :");
      gender = userInput.next();
      System.out.println("Lulusan :");
      lulusan = userInput.next();
      System.out.println("Pengalaman (Tahun) :");
      pengalaman = userInput.nextInt();
      System.out.println("Penampilan (1/10) :");
      penampilan = userInput.nextDouble();
      System.out.println("Posisi Yang Dilamar");
      System.out.print("(SPV, Admin, Or Koordinator) : ");
      posisi = userInput.next();

      /*OUTPUT RESULT*/
      System.out.println();
      System.out.println("///////////////////////////////////////////////////////////////////////////////");
      System.out.println();
      
      /*CALLING FUNCTION RESULT(HASIL)*/
      hasil(nama, posisi);

      	/*KONDISI*/
      	if(posisi.equals("Koordinator")){
      		if(koordinator(umur, pengalaman, penampilan, lulusan, gender)){
      			lolos(nama, posisi);
      		}else{
      			gagal(nama, posisi);
      		}
      	}else if(posisi.equals("Admin")){
      		if(admin(umur, pengalaman, penampilan, lulusan, gender)){
      			lolos(nama, posisi);
      		}else{
      			gagal(nama, posisi);
      		}
      	}else if(posisi.equals("SPV")){
      		if (spv(umur, pengalaman, penampilan, lulusan, gender)){
      			lolos(nama, posisi);
      		}else{
      			gagal(nama, posisi);
      		}
      	}
    }

		}



		////////////////////
		/*FUNCTION SECTION*/
		////////////////////

		// FUNCTION UNTUK KOORDINATOR
		public static boolean koordinator(int umur, int pengalaman, double penampilan, String lulusan, String gender){
			/*
				KOORDINATOR

				Kriteria 1
				1. Pria
				2. Umur minimal 20 tahun dan maksimal  30 tahun
				3. Lulusan SMK/D3
				4. Memiliki pengalaman minimal 2 tahun

				Kriteria 2
				1. Pria
				2. Umur 30 tahun keatas
				3. Lulusan S1
				4. Memiliki pengalaman minimal 5 tahun
			*/

			boolean kriteria_1, kriteria_2, result;

			kriteria_1 = ((gender.equals("Pria") && (umur >= 20) && (umur <= 30) && ((lulusan.equals("SMK")) || (lulusan.equals("D3"))) && (pengalaman >= 2)));
			kriteria_2 = (gender.equals("Pria") && (umur >= 30) && lulusan.equals("S1") && pengalaman >= 5);

			if (kriteria_1 || kriteria_2){
				result = true;
			}else{
				result = false;
			}
			return result;
		}

		// FUNCTION UNTUK ADMIN
		public static boolean admin(int umur, int pengalaman, double penampilan, String lulusan, String gender){
			/*
				ADMIN

				Kriteria 1
				1. Wanita
				2. Umur minimal 20 tahun dan maksimal 25 tahun
				3. Lulusan D3
				4. Memiliki penampilan yang "Menarik" atau pengalaman minimal 1 tahun

				Kriteria 2
				1. Wanita
				2. Umur diatas 25 tahun
				3. Lulusan S1
				4. Memiliki penampilan yang "Menarik"
				5. Memiliki pengalaman minimal 3 tahun

				Kriteria 3
				1. Pria
				2. Umur dari 20 tahun sampai 30 tahun maksimal
				3. Memiliki penampilan "Menarik"
				4. Lulusan D3/S1
				5. Memiliki pengalaman kerja minimal 2 tahun
			*/

			boolean kriteria_1, kriteria_2, kriteria_3, result;

			kriteria_1 = (gender.equals("Wanita")) && ((umur >= 20) && (umur <= 25)) && (lulusan.equals("D3")) && ((penampilan >= 8.5) || (pengalaman >= 1));
			kriteria_2 = (gender.equals("Wanita") && umur >= 25 && lulusan.equals("S1") && penampilan >= 8.5 && pengalaman >= 3);
			kriteria_3 = (gender.equals("Pria") && ((umur >= 20) && (umur <= 30)) && (penampilan >= 8.5) && ((lulusan.equals("D3")) || (lulusan.equals("S1"))) && (pengalaman >= 2));

			if(kriteria_1 || kriteria_2 || kriteria_3){
				result = true;
			}else {
				result = false;
			}

			return result;
		}

		// FUNCTION UNTUK SPV
		public static boolean spv(int umur, int pengalaman, double penampilan, String lulusan, String gender){
			/*
				SPV

				Kriteria 1
				1. Pria atau Wanita
				2. Umur minimal 23 tahun sampai maksimal 30 tahun
				3. Lulusan S1
				4. Memiliki pengalaman kerja minimal 1 tahun

				Kriteria 2
				1. Pria atau Wanita
				2. Umur minimal 25 tahun sampai maksimal 35 tahun
				3. Lulusan D3
				4. Memiliki pengalaman kerja lebih dari 4 tahun
			*/

			boolean kriteria_1, kriteria_2, result;

			kriteria_1 = ((gender.equals("Pria")) || (gender.equals("Wanita"))) && ((umur >= 23) && (umur <= 30)) && (lulusan.equals("S1")) && (pengalaman >= 1);
			kriteria_2 = (gender.equals("Pria") || (gender.equals("Wanita"))) && ((umur >= 25) && (umur <= 35)) && (lulusan.equals("D3")) && (pengalaman >= 4);

			if(kriteria_1 || kriteria_2){
				result = true;
			}else{
				result = false;
			}

			return result;
		}

		// FUNCTION RESULT(HASIL)
		static void hasil(String nama, String posisi){
			System.out.println(nama + ", terima kasih sudah mengikuti lowongan kerja di PT. Secret Semut.");
			System.out.println();
			System.out.println("Berikut Hasil dari Rekrutment untuk posisi " + posisi + " :");
		}

		// FUNCTION JIKA DITERIMA
		static void lolos(String nama, String posisi){
			System.out.println("Selamat, " + nama + ".");
			System.out.println("Anda memenuhi syarat sebagai " + posisi + ". Dan akan masuk ke Tahap Selanjutnya");
		}

		// FUNCTION JIKA TIDAK DITERIMA
		static void gagal(String nama, String posisi){
			System.out.println("Maaf, " + nama);
			System.out.println("Anda Tidak memenuhi syarat untuk sebagai " + posisi + ".");
			System.out.println("Dan Tidak akan masuk ke Tahap Selanjutnya.");
		}

		
}